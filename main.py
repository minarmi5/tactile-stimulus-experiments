import os
import shutil
import random
import pygame
import serial

from src import Participant, add_participant
from src import Block, BlockType, BlockBuzzingMode
from src import Buzzers
from src import config
from src import Color

# Global variables
active_participant: Participant = None
running: bool = True
buzzers: Buzzers = None


def clear_screen() -> None:
    # cross-platform solution
    os.system("cls" if os.name == "nt" else "clear")


def print_underlined(msg: str, char: str = "-", color: str = None) -> None:
    l = len(msg)
    if color is not None:
        print(f"{color}{msg}{Color.END}")
    else:
        print(msg)
    print(l * char)


def print_header() -> None:
    msg = f"Touch localization in adults"
    print_underlined(msg, "=", Color.GREEN)

    # Print config
    print(f"{Color.BOLD}Experiment configuration (loaded from `{Color.YELLOW}src/config.py{Color.END}`){Color.END}\n")
    config.print_config()

    # Print current active participant if selected
    if active_participant is not None:
        print(len(msg) * "-")  # match the = above
        print(
            f"{Color.BOLD}Selected participant: {Color.BLUE}{active_participant.participant_id}{Color.END}{Color.END}\n"
        )

        # Print the distribution of the buzzers
        print(f"{Color.BOLD}Buzzers{Color.END}")
        buzzer_dist = active_participant.left_arm_length / (config.buzzer_count - 1)
        print(f"\t{config.buzzer_count} buzzers on {active_participant.left_arm_length:.2f} cm", end="")
        print(f" -> 1 buzzer every {buzzer_dist:.2f} cm")
        print(f"\tDistribution [cm]:\t", end="")
        print(*map(lambda i: f"{i * buzzer_dist:4.2f}", range(config.buzzer_count)), end="\n\n")

        # Show buzzers on landmarks
        print(
            f"\tLandmarks:\t"
            + f"\t1 - {Color.BOLD}{active_participant.landmark_1_buzzer_id}{Color.END}"
            + f"\t2 - {Color.BOLD}{active_participant.landmark_2_buzzer_id}{Color.END}"
            + f"\t3 - {Color.BOLD}{active_participant.landmark_3_buzzer_id}{Color.END}"
            + f"\t4 - {Color.BOLD}{active_participant.landmark_4_buzzer_id}{Color.END}",
            end="\n\n",
        )
        # Print blocks performed for this participant
        active_participant.print_blocks()

    print(len(msg) * "=")  # match the = above


def add_new_participant() -> None:
    global active_participant

    while True:
        print_underlined("Adding a new participant", "-", Color.BLUE)
        p = add_participant()

        choice = input(f"\nIs the information correct [{Color.GREEN}y{Color.END}/{Color.RED}n{Color.END}]? ")
        if choice.lower() == "y":
            break

        print("Information is not correct, trying again ...\n")

    p.save_to_csv(f"{config.results_dir}/{p.participant_id}/info.csv")
    active_participant = p


def select_existing_participant() -> None:
    global active_participant

    while True:
        print_underlined("Selecting an existing participant", "-", Color.BLUE)

        # Get list of all available participants as the folders inside
        # the result folder which contain file "info.csv"

        entries = os.listdir(config.results_dir)
        available = [e for e in entries if os.path.exists(f"{config.results_dir}/{e}/info.csv")]

        if len(available) == 0:
            print("No participants available.")
            input("Press enter to continue.")
            return

        print(f"Available participant ids: {available}")

        choice = input("Your choice (case sensitive) ['-' to exit]: ")
        print("")

        if choice == "-":
            return

        if choice in available:
            info_path = f"{config.results_dir}/{choice}/info.csv"
            break

        print("Invalid choice, please try again ...\n")

    active_participant = Participant()
    success = active_participant.load_from_csv(info_path, verbose=True)
    if not success:
        print(f"The participant could not be loaded. Fix '{info_path}' and try again.")
        input("Press enter to continue.")
        active_participant = None
        return

    choice = input(f"\nIs the information correct [{Color.GREEN}y{Color.END}/{Color.RED}n{Color.END}]? ")
    if choice.lower() != "y":
        print(f"The information is not correct. Fix '{info_path}' and try again.")
        input("Press enter to continue.")
        active_participant = None


def _choose_block_type() -> str:
    available_types = [BlockType.SHORT, BlockType.CONTINUOUS]
    available_type_names = ["Short", "Continuous"]
    type_choice = None
    while type_choice is None:
        print("Select the block type", end=" ")
        print(
            ", ".join([f"'{Color.BOLD}{x}{Color.END}' - {y}" for x, y in zip(available_types, available_type_names)]),
            end="",
        )
        print(":", end=" ")
        type_choice = input()

        if type_choice not in available_types:
            print("Invalid choice, try again")
            type_choice = None
            continue

    return type_choice

def _choose_block_mode() -> str:
    available_modes = [BlockBuzzingMode.PULSES, BlockBuzzingMode.VIBRATIONS]
    available_mode_names = ["Pulses", "Vibrations"]
    mode_choice = None
    while mode_choice is None:
        print("Select the block buzzing mode", end=" ")
        print(
            ", ".join([f"'{Color.BOLD}{x}{Color.END}' - {y}" for x, y in zip(available_modes, available_mode_names)]),
            end="",
        )
        print(":", end=" ")
        mode_choice = input()

        if mode_choice not in available_modes:
            print("Invalid choice, try again")
            mode_choice = None
            continue

    return mode_choice


def run_practice_block() -> None:
    print_underlined("Running a practice block", "-", Color.BLUE)

    # Let the user choose the type of the block
    type_choice = _choose_block_type()

    # Let the user choose the buzzing mode of the block
    mode_choice = _choose_block_mode()

    # Create a new block and initialize it
    block = Block()
    success = block.initialize_new_block(
        participant_id=active_participant.participant_id,
        block_id=0,
        block_type=type_choice,
        block_buzzing_mode=mode_choice,
        total_trials=5,
        seed=0,
        save=False,
    )

    if not success:
        input("Initialization of the practice block failed. Press enter to continue.")
        return

    # Run the block. All data is saved during the block's execution
    input(
        f"Practice block with 5 trials for participant {active_participant.participant_id} is ready, press enter to start ..."
    )
    success = block.run_block(buzzers, save=False)
    if not success:
        print(f"Running practice block for participant {active_participant.participant_id} failed.")
        input("Press enter to continue.")
        return

    print(f"Practice block for participant {active_participant.participant_id} finished successfuly.")
    input("Press enter to continue.")


def run_specific_block() -> None:
    """
    The user selects which block of trials to run.

    It can be either a new block or block that is already completed.
    If the block already exists in the result directory, it's
    folder will be deleted.
    """
    print_underlined("Running a specific trial block", "-", Color.BLUE)

    # Select which block does the user want to run / rerun
    id_choice = None
    while id_choice is None:
        id_choice = input(f"Which block do you want to run [1-4] ['-' to exit]: ")

        if id_choice == "-":
            return

        # Parse the choice
        try:
            id_choice = int(id_choice)
        except Exception as e:
            print("Invalid choice, try again")
            id_choice = None
            continue

        if id_choice < 1 or id_choice > 4:
            print("Invalid choice, try again")
            id_choice = None
            continue

    # Check if the block exists to warn before deleting the corresponding folder
    blocks = active_participant.get_blocks()
    block_ids = [b.block_id for b in blocks]
    confirm = None
    if id_choice in block_ids:
        block_dir = f"{config.results_dir}/{active_participant.participant_id}/block_{id_choice}"
        print(
            f"Block {id_choice} was interrupted or is already completed. "
            + f"Running the block now will delete all previous contents of the folder `{block_dir}`"
        )
        confirm = input(
            f"Do you really want to run block {id_choice} from the beginning [{Color.GREEN}y{Color.END}/{Color.RED}n{Color.END}]? "
        )
        if confirm.lower() != "y":
            print(
                f"The block {id_choice} will not start. In case you wanted to resume an interrupted run, choose 'Resume an interrupted block' in the main menu."
            )
            input("Press enter to continue.")
            return

        # Delete the folder
        shutil.rmtree(block_dir)

    # Let the user choose the type of the block
    type_choice = _choose_block_type()

    # Let the user choose the buzzing mode of the block
    mode_choice = _choose_block_mode()

    # Let the user set the seed or leave it empty for random seed
    seed_choice = None
    while seed_choice is None:
        seed_choice = input(f"Set the seed (positive integer) or leave empty for a random seed: ")

        # Set random seed if empty
        if seed_choice == "":
            # NOTE: setting the max seed to 10000 results in generating
            # at maximum 10000 unique buzzer orders. This should be enough though
            seed_choice = random.randint(1, 10000)
            break

        try:
            seed_choice = int(seed_choice)
        except Exception as e:
            print("Invalid seed, try again (seed has to be a positive integer)")
            seed_choice = None
            continue

        if seed_choice < 0:
            print("Invalid seed, try again (seed has to be a positive integer)")
            type_choice = None
            continue

    print("")  # New line for better readability

    # Create a new block and initialize it
    block = Block()
    success = block.initialize_new_block(
        participant_id=active_participant.participant_id,
        block_id=id_choice,
        block_type=type_choice,
        block_buzzing_mode=mode_choice,
        total_trials=config.trials_in_one_block,
        seed=seed_choice,
    )

    if not success:
        input("Initialization of the block failed. Press enter to continue.")
        return

    # Run the block. All data is saved during the block's execution
    input(f"Block {id_choice} for participant {active_participant.participant_id} is ready, press enter to start ...")
    success = block.run_block(buzzers)
    if not success:
        print(f"Running block {id_choice} for participant {active_participant.participant_id} failed.")
        input("Press enter to continue.")
        return

    print(f"Block {id_choice} for participant {active_participant.participant_id} finished successfuly.")
    input("Press enter to continue.")


def resume_interrupted_block() -> None:
    """
    Lets the user select a block from his available blocks where
        completed_trials < total_trials
    """
    block_list = active_participant.get_blocks()
    blocks = {b.block_id : b for b in block_list if b.completed_trials < b.total_trials}

    if len(blocks) == 0:
        print("No blocks to resume.")
        input("Press enter to continue.")
        return

    # Select which block does the user want to run / rerun
    id_choice = None
    while id_choice is None:
        id_choice = input(f"\nWhich block do you want to resume {list(blocks.keys())} ['-' to exit]: ")

        if id_choice == "-":
            return

        # Parse the choice
        try:
            id_choice = int(id_choice)
        except Exception as e:
            print("Invalid choice, try again")
            id_choice = None
            continue

        if id_choice not in blocks.keys():
            print("Invalid choice, try again")
            id_choice = None
            continue

    # Run the remaining trials
    success = blocks[id_choice].run_block(buzzers)

    if not success:
        print(f"Running block {id_choice} for participant {active_participant.participant_id} failed.")
        input("Press enter to continue.")
        return

    print(f"Block {id_choice} for participant {active_participant.participant_id} finished successfuly.")
    input("Press enter to continue.")


def create_options() -> list:
    """
    If no participant is selected, allow only adding
    or selecting an existing participant.
    Otherwise allow everything
    """
    options = [
        ("Add a new participant", add_new_participant),
        ("Select an existing participant", select_existing_participant),
    ]

    if active_participant is not None:
        options += [
            ("Run a practice block", run_practice_block),
            ("Run a specific block", run_specific_block),
            ("Resume an interrupted block", resume_interrupted_block),
        ]

    options += [
        ("Exit", exit),
    ]

    return options


def print_options(options: list) -> None:
    """
    Prints `options` (list containing [(id, msg), ...]) one below the other
    """
    for id, option in enumerate(options, start=1):
        print(f"[{Color.CYAN}{id}{Color.END}] {Color.BOLD}{option[0]}{Color.END}")


if __name__ == "__main__":
    # Initialize pygame mixer for playing sounds
    pygame.mixer.init()

    # First of all, try to connect to the buzzers. If it is not possible, do not continue
    print("Connecting to the Buzzer board ...")
    try:
        buzzers = Buzzers(test=False)
        # buzzers = Buzzers(test=True)  # NOTE: Only for testing, otherwise set test=False
    except serial.serialutil.SerialException as e:
        print(f"Error occured while connecting to the board: {e}")
        print(f"Did you call {Color.BOLD}`sudo rfcomm bind 0 00:14:01:03:38:FC 1` {Color.END}?")
        exit(0)

    except Exception as e:
        print(f"Error occured while connecting to the board: {e}")
        exit(0)

    success = buzzers.check_connection()
    if not success:
        print(f"Unable to connect to the buzzer, exiting")
        exit(0)

    clear_screen()

    # For now, running is always True and the program exits when "Exit" is selected,
    # calling the python build-in method exit().
    while running:
        clear_screen()
        print_header()

        options = create_options()
        print_options(options)

        choice = None
        while choice is None:
            choice = input(f"Your choice [{Color.CYAN}1 - {len(options)}{Color.END}]: ")

            # Parse the choice
            try:
                choice = int(choice)
            except Exception as e:
                print("Invalid choice, try again")
                choice = None
                continue

            if choice < 1 or choice > len(options):
                print("Invalid choice, try again")
                choice = None
                continue

        print("")  # New line for better readability

        # Options are 1-indexed, therefore 1 needs to be subtracted
        # The options are in the form (description, function)
        # [1]() results in function()
        options[choice - 1][1]()

        print("")  # New line for better readability
