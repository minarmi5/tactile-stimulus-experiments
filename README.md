# Tactile Stimulus Experiments

Collection of scripts used to control the experiments of the project **Localization of temporary and continuous touch in adults**.

|                                                     |
| --------------------------------------------------- |
| :exclamation: Keep the data folder on the qualisys laptop where the recorded trials are saved **empty**, otherwise the qualisys system gets slow and a lot of timeouts will start to occur  :exclamation:           |
|                                                     |

## Table of contents

[1. Setup](#1-setup)

&ensp;[1.1 Connecting to the buzzers](#11-connecting-to-the-buzzers)

&ensp;[1.2 Qualisys system setup](#12-qualisys-system-setup)

&ensp;[1.3 Experiment configuration](#13-experiment-configuration)

[2. Usage](#2-usage)

&ensp;[2.1 Add a new participant](#21-add-a-new-participant)

&ensp;[2.2 Select an existing participant](#22-select-an-existing-participant)

&ensp;[2.3 Run a practice block](#23-run-a-practice-block)

&ensp;[2.4 Run a specific block](#24-run-a-specific-block)

&ensp;[2.5 Resume an interrupted block](#25-resume-an-interrupted-block)

[3. Trial timing](#3-trial-timing)

&ensp;[3.1 Short block type](#31-short-block-type)

&ensp;[3.2 Continuous block type](#32-continuous-block-type)

[4. Data and results](#4-data-and-results)

&ensp;[4.1 Output](#41-output)

[5. Authors](#5-authors)

<div style="page-break-after: always;"></div>

## 1. Setup

Clone this repository by running

```bash
git clone git@gitlab.fel.cvut.cz:minarmi5/tactile-stimulus-experiments.git
```

and setup the python environment by
```bash
pip3 install -r requirements.txt
```

This project was tested with **Python 3.8.16** on **Ubuntu 22.04**.

### 1.1 Connecting to the buzzers

The buzzer are controlled using a Raspberry Pi controller, which can be connected via Bluetooth.
The board can be **switched on** by **clicking the black button on the side** and **switched off** by a **double-click**.
When the green LED is on and red LED is blinking quickly, the device is ready to pair. 

First, on the local PC, add the user to the group `dialout`, so that the user has rights to access the bluetooth interface.
```sh
sudo vim /etc/group
``` 
find the `dialout` group and add the user's name
```sh
dialout:x:20:<user>
```
**PC needs to be restarted for this change to take effect**.

Then, connect the local PC to the bluetooth device (if not working, see [Note below](#111-bluetooth-pairing-note))
```
name: Buzzer
MAC:  00:14:01:03:38:FC
PIN:  1234
```

after connecting to the device, specify the correct bluetooth interface by

```sh
sudo rfcomm bind 0 00:14:01:03:38:FC 1
```

Python package **PyCmdMessenger** is used to communicate with the microcontroller.
The microcontroller allows to set the PWM level (**0.0** - off, **1.0** - full power).
See [src/buzzers.py](src/buzzers.py) for more info.

To test the buzzers, run `test.py` and select the `"Buzz buzzers"` option.

#### 1.1.1 Bluetooth pairing note

If the pairing does not work through the Ubuntu settings (which happened to me on Ubuntu 22.04), here is the method which made it work:

1. Ensure all bluetooth packages are installed correctly
```bash
sudo apt install bluetooth bluez bluez-tools rfkill
```

2. Check whether `hcitool` sees the device (if first run shows nothing, try a few more times)
```sh
hcitool scan
```
You should see
```sh
	00:14:01:03:38:FC	Buzzers
```

3. Connect and pair the board using `bluetoothctl`

```bash
bluetoothctl

[bluetooth]# connect 00:14:01:03:38:FC
```

### 1.2 Qualisys system setup

Connecting to the cameras and initializing remote control of the Qualisys Track Manager works as follows.
The QTM needs to be run on the **qualisys ntb** (Dell laptop with **Qualisys** logo), to which the cameras are connected.
The device running the `main.py` script needs to be connected to the **qualisys ntb** through an ethernet connection, creating a local network.
For this, a dock (Lenovo ThinkPad Thunderbolt 3 Dock Gen 2, black box with **ThinkPad** logo) is used (the only purspose is to supply another ethernet port, since the **qualisys ntb** has only one which is used for the cameras). 

Start with the **qualisys ntb** turned **off**, **main ntb** turned **on** and proceed as follows:

1. **dock**
   1. Connect the **dock** to the power outlet
   2. Connect the **dock** and **qualisys ntb** via the USB-C to USB-C cable
   3. Connect the **dock** and **main ntb** via the Ethernet to Ethernet cable
2. **cameras** (black power injector - two ethernet-like ports on one side, one on the other)
   1. Plug the AC power adapter to the power outlet.
   2. Plug the white ethernet cable to the **qualisys ntb** (to the ethernet port labeled **Cameras**).
3. **qualisys ntb**
   1. Turn on the ntb
   2. Ensure that the dock is connected properly and the local network is active (see [note below](#121-checking-the-dock-connection-on-the-qualisys-ntb))
   3. Start QTM and select project
4. **main ntb**
   1. Ensure that the local ethernet connection is active (see [note below](#122-checking-the-local-ethernet-connection-is-active-on-the-main-ntb))
   2. Check that `config.qualisys_ntb_ip` is set properly (see [note below](#121-checking-the-dock-connection-on-the-qualisys-ntb))
   
To test the qualisys system, run `test.py` and select the `"Test Qualisys connection"` option.

#### 1.2.1 Checking the dock connection on the qualisys ntb

The dock is used to connect the **qualisys ntb** and **main ntb** via a local network (since the **qualisys ntb** has only one ethernet port which is used for the cameras, a dock is needed).
To check whether the dock is connected properly and the local network is active, go to 
**Settings** -> **Network & Internet** -> **View hardware and connection properties**.

There should be an entry
```
Name:			Ethernet
Description:	Lenovo USB Ethernet
Status:			Operational
IPv4 address:	xxx.xxx.xxx.xxx
```

If the status is not **Operational**, ensure that:
1. **dock** is connected to the power outlet and is on (glows orange)
2. **dock** is connected to the **qualisys ntb** via the USB-C to USB-C cable
3. **dock** is connected to the **main ntb** via the Ethernet to Ethernet cable

If everything is connected properly, reboot the **qualisys ntb** and check again.

When the status is **Operational**, note the IPv4 address. This is the address to be set in `config.qualisys_ntb_ip`.

#### 1.2.2 Checking the local ethernet connection is active on the main ntb

(**NOTE:** written for **UBUNTU 22.04**)

In the system status dropdown (click status icons at the top right corner of your screen), you should see **Wired Connected**.
However, it may happen that the **main ntb** does not accept the wired connection, since no internet connection is available.
In that case, go to **Settings** -> **Network** -> **Wired** settings (gear icon).
1. In **Identity** tab, select some available MAC address if none is selected.
2. In **IPv4** and **IPv6** tabs, select **Link-Local Only**.

The connection should work now.

### 1.3 Experiment configuration

To configure the experiments, change the values inside [src/config.py](src/config.py).

<div style="page-break-after: always;"></div>


## 2. Usage

To run the experiment script, launch [main.py](main.py).

A main menu is presented with options to either add a new participant or select an existing one.
Participants are stored inside a folder `config.results_dir` specified in [src/config.py](src/config.py).

- [Add a new participant](#21-add-a-new-participant)
- [Select an existing participant](#22-select-an-existing-participant)

After a participant is added or selected, another options appear.

- [Run a practice block](#23-run-a-practice-block)
- [Run a specific block](#24-run-a-specific-block)
- [Resume an interrupted block](#25-resume-an-interrupted-block)

### 2.1 Add a new participant

To add a new participant, choose **Add a new participant** in the main menu.
The user is then asked for several pieces of information:

```
participant id					# String of characters, will identify the participant
name							# Full name of the participant
gender (F/M)					# Female / Male
age								# Years of age
profession						
activity per week [h]			# Physical activity (sports, training, ...)
handedness (R/L/A)				# Right / Left / Ambidexterous
left hand length [cm]			
left forearm length [cm]
left upper arm length [cm]
left arm length [cm]			# Defaults to the sum of the three above
note							# Any other notes
```

**No input checking** is done! The user needs to verify himself that the values are correct.

After the user confirms the validity of the data, a new folder `<participant_id>` is created in `config.results_dir` and `info.csv` is saved to this folder (see [info.csv](#411-infocsv)).

### 2.2 Select an existing participant

To select an existing participant, choose **Select an existing participant** in the main menu.

This brings up selection the available participants from `config.results_dir`.

### 2.3 Run a practice block

To run a practice block for chosen participant, choose **Run a practice block** in the main menu.

The user selects block of which type will be run and then 10 trials are run for the participant.
Nothing is saved from this practice block.

### 2.4 Run a specific block

To run a specific block for chosen participant, choose **Run a specific block** in the main menu.

Here, the user selects
- which block will be run (1-4)
- type of the block (**short** or **continuous**)
- seed for the block (this ensures repeatability, can be left empty to generate random seed)

The results are saved after each trial.
In the case that the block is interrupted, it can be afterwards resumed by ["Resume an interrupted block"](#25-resume-an-interrupted-block).

### 2.5 Resume an interrupted block

To run an interrupted block for chosen participant, choose **Run an interrupted block** in the main menu.

A selection of blocks currently in progress is brought up.
An interrupted block is defined as a block in which `completed_trials` is less then `total_trials`. 

<div style="page-break-after: always;"></div>

## 3. Trial timing

The timing of the trials is crucial for this experiment, this section explains how the trials are timed.

At the start of each trial a sound is played, specified in `config.start_trial_sound_file`.
The sound is played in a non-blocking mode, meaning the trial starts as soon as the sound starts. If you
want to start the trial during or after the sound, specify this delay in `config.start_trial_sound_duration`.
When the trial starts, event `config.qtm_event_trial_start` is sent to the qualisys system and `trial.trial_start_dt` is set
to the current datetime.

After the start of the trial, there is a short delay after which the buzzers start buzzing.
This delay is selected randomly from range specified in `config.buzzer_delay_range`.

As the buzzer is connected via bluetooth, there is some **latency** between **sending the command to buzz**, the **buzzer starting to buzz** and the buzzer board **sending a response**.
These events are captured in the resulting csv files (see [4.1.3 block/trials.csv](#413-blocktrialscsv)).
The events are also send to the qualisys system (`config.qtm_event_buzzer_start_sent`, `config.qtm_event_buzzer_start_received`, ...).

The trial then proceeds based on the block type.

### 3.1 Short block type

In the short block type, the buzzer is stopped after a while and only then can the participant start his motion (the events BUZZER_STOP and MOVEMENT_START will coincide).

**NOTE:** The event BUZZER_START is actually split to two events, BUZZER_START_SENT and BUZZER_START_RECEIVED as described above.
However, in the following visualizations, we keep the event as single time instance for better clarity.

```
events:    TRIAL_START BUZZER_START BUZZER_STOP=MOVEMENT_START TRIAL_STOP
							 |>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>|
			 ***|============|:::::::::::::::::|                    |/////
recording: |--------------------------------------------------------------|
			    |            | 	               |                    |
sounds:      |-start-|                         |-move-|             |-end-|
			    |            |			       |                    |
stimulus:                    |-----------------| 
			    |            |			       |                    |
movement:                                      |--------------------|


>>> config.trial_duration
*** config.start_trial_sound_duration
=== delay from config.buzzer_delay_range
::: config.buzzer_duration_short
/// config.end_trial_sound_duration
```

<div style="page-break-after: always;"></div>

### 3.2 Continuous block type

In the short block type, the buzzer keeps buzzing until the end of the trial (the events BUZZER_START and MOVEMENT_START will coincide, as well as BUZZER_STOP and TRIAL_STOP).

**NOTE:** The event BUZZER_START is actually split to two events, BUZZER_START_SENT and BUZZER_START_RECEIVED as described above.
However, in the following visualizations, we keep the event as single time instance for better clarity.

```
events:    TRIAL_START BUZZER_START=MOVEMENT_START BUZZER_STOP=TRIAL_STOP
			 ***|==================|>>>>>>>>>>>>>>>>>>>>>>>>>>|/////
recording: |------------------------------------------------------------|
			    |             	   |                          |           
sounds:      |-start-|             |-move-|                   |-end-|
			    |                  |                          |
stimulus:                          |--------------------------| 
			    |            	   |                          |
movement:                          |--------------------------|


>>> config.trial_duration
*** config.start_trial_sound_duration
=== delay from config.buzzer_delay_range
/// config.end_trial_sound_duration
```

<div style="page-break-after: always;"></div>

## 4. Data and results
 
### 4.1 Output

Data recorder during the experiments are saved to the `config.results_dir` directory.
For each participant a directory is generated with the participant's ID as the directory name.
The folder contains `info.csv`, holding information about the participant and a folder for each block that was run for this participant.
Each block contains another `info.csv` file with information about the block and `trials.csv` with info about the trials.
For each trial, a `.tsv` file in saved **on the qualisys notebook** with the recorded movement.

```
participant/
	info.csv
	block_1/
		info.csv
		trials.csv
	block_2/
		...
	...
```

#### 4.1.1 info.csv

Contains information about the participant.

It is a csv with the following columns
```
participant id
name
gender (F/M)
age
profession
activity per week [h]
handedness (R/L/A)
left hand length [cm]
left forearm length [cm]
left upper arm length [cm]
left arm length [cm]
note
```

See [data/example/TEST/info.csv](data/example/TEST/info.csv) for an example of the participant info file.

<div style="page-break-after: always;"></div>

### 4.1.2 block/info.csv

Contains information about the block.

It is a csv with the following columns
```
block id
block type
completed trials
total trials
start datetime
end datetime
seed
```

See [data/example/TEST/block_1/info.csv](data/example/TEST/block_1/info.csv) for an example of the block info file.

### 4.1.3 block/trials.csv

Contains information about the trials run in this block.

It is a csv with the following columns
```
trial id
buzzer id
trial start
trial end
buzzer start sent
buzzer start received
buzzer stop
movement allowed
trial seed
```

See [data/example/TEST/block_1/trials.csv](data/example/TEST/block_1/trials.csv) for an example of the trials file.

## 5. Authors

Michal Minařík (minarmi5@fel.cvut.cz)