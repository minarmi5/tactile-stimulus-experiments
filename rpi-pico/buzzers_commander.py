import PyCmdMessenger
import time as t


class Buzzer_commander():
    def __init__(self, port = "/dev/rfcomm0",baud_rate = 115200):

        #Don't forget to call "sudo rfcomm bind 0 00:14:01:03:38:FC 1"
        arduino = PyCmdMessenger.ArduinoBoard(port,baud_rate=baud_rate)
        self.commands = [["buzz","sfff"],
                         ["impulse","sif"],
                         ["toggle",'b'],
                         ["response","b"]]

        self.channels = ['A1','A2','A3','A4','B1','B2','B3','B4','C1','C2','C3','C4','D1','D2','D3','D4']

        self.b = PyCmdMessenger.CmdMessenger(arduino,self.commands)
        t.sleep(1)
        print("Ready to command buzzers")
     
    def set_vibration(self, channel, amplitude, frequency, duration):
        if channel not in self.channels:
            print("Error, invalid channel name")
        elif (type(amplitude) != float and type(amplitude) != int) or amplitude < 0:
            print("Error, amplitude must be non-negative number")
        elif (type(frequency) != float and type(frequency) != int) or frequency < 1 or frequency > 1000:
            print("Error, frequency must be number in range [1,1000] Hz")
        elif (type(duration) != float and type(duration) != int) or duration < 0:
            print("Error, duration must be non-negative number")
        else:
            self.b.send("buzz",channel,amplitude,frequency,duration)
            self.check_response() 

    def set_impulses(self, channel, count, period):
        if channel not in self.channels:
            print("Error, invalid channel name")
        elif (type(count) != int) or count <= 0:
            print("Error, number of pulses must be positive integer")
        elif (type(period) != float and type(period) != int) or period <= 0 or period > 1:
            print("Error, pulse period must be number in range (0,1] seconds")
        else:
            self.b.send("impulse", channel, count, period)
            self.check_response() 

    def check_response(self):
        msg = self.b.receive()
        if (msg==None):
            print("NO RESPONSE FROM CONTROLLER")
        else:
            if (msg[1][0] != 1):
                print("AN ERROR OCCURED DURING COMMINICATION")
            else:
                pass


if __name__ == "__main__":
    # Demo: sets Vibration for all buzzers and then impulses
    commander = Buzzer_commander()

    # Wrong commands
    print('wrong channel example')
    commander.set_vibration('Alik', 1, 200, 3) 
    print()
    t.sleep(1)

    print('wrong amplitude example')
    commander.set_vibration('A1', 'A1', 200, 3) 
    print()
    t.sleep(1)

    print('wrong frequency example')
    commander.set_vibration('A1', 0.9, 'lol', 3) 
    print()
    t.sleep(1)

    print('wrong duration example')
    commander.set_vibration('A1', 0.9, 200, -1) 
    print()
    t.sleep(1)

    print('wrong number of impulses')
    commander.set_impulses('A1', -2, 0.3) 
    print()
    t.sleep(1)

    print('wrong period')
    commander.set_impulses('A1', -2, -0.1) 
    print()
    t.sleep(1)


    for i in range(16):
        commander.set_vibration(commander.channels[i],0.9,200,3)
        t.sleep(4)
        commander.set_impulses(commander.channels[i],3,0.3)
        t.sleep(1)
        
