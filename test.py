import os
import qtm
import time
import pygame
import asyncio

from datetime import datetime

from src import config
from src import Buzzers
from src import Color

running: bool = True


def clear_screen() -> None:
    # cross-platform solution
    os.system("cls" if os.name == "nt" else "clear")


def print_underlined(msg: str, char: str = "-", color: str = None) -> None:
    l = len(msg)
    if color is not None:
        print(f"{color}{msg}{Color.END}")
    else:
        print(msg)
    print(l * char)


def print_header() -> None:
    msg = "Touch localization in adults - TESTS"
    print_underlined(msg, "=", Color.GREEN)


def print_options(options: list):
    for id, option in enumerate(options, start=1):
        print(f"[{Color.CYAN}{id}{Color.END}] {Color.BOLD}{option[0]}{Color.END}")


def play_sounds() -> None:
    # Initialize pygame mixer for playing sounds
    pygame.mixer.init()

    print(f"Start trial (loaded from {config.start_trial_sound_file})")
    start_dt = datetime.now()
    pygame.mixer.music.load(config.start_trial_sound_file)
    pygame.mixer.music.play()
    time.sleep(1.875)
    print("Now")
    end_dt = datetime.now()
    print(f"Sound duration: {(end_dt-start_dt).total_seconds()} s")
    time.sleep(1)
    # return

    print(f"Start movement (loaded from {config.start_movement_sound_file})")
    start_dt = datetime.now()
    pygame.mixer.music.load(config.start_movement_sound_file)
    pygame.mixer.music.play()
    end_dt = datetime.now()
    print(f"Sound duration: {(end_dt-start_dt).total_seconds()} s")
    time.sleep(1)

    print(f"End trial (loaded from {config.end_trial_sound_file})")
    start_dt = datetime.now()
    pygame.mixer.music.load(config.end_trial_sound_file)
    pygame.mixer.music.play()
    end_dt = datetime.now()
    print(f"Sound duration: {(end_dt-start_dt).total_seconds()} s")

    input("Press enter to exit")


def test_buzzers() -> None:
    """
    Goes through the buzzers in order defined in `config.buzzer_ids`
    """
    print("Connecting to the Buzzer board ...")
    try:
        buzzers = Buzzers()
    except Exception as e:
        buzzer = None
        print(f"Error occured while connecting to the board: {e}")
        exit(0)

    success = buzzers.check_connection()
    if not success:
        print(f"Unable to connect to the buzzer, exiting")
        exit(0)

    amplitude = config.buzzer_vibration_amplitude
    frequency = config.buzzer_vibration_frequency
    period = config.buzzer_pulse_period

    vibration_duration = 0.3 # [s]
    pulse_count = 4

    for buzzer_id in config.buzzer_ids:
        print(f"Testing buzzer {buzzer_id} ...")

        # Vibrate
        success = buzzers.set_vibration(buzzer_id, amplitude, frequency, vibration_duration, True)
        if not success:
            print(f"buzzers.set_vibration({buzzer_id}, {amplitude}, {frequency}) failed")
        time.sleep(vibration_duration)

        # Pulse
        success = buzzers.set_impulses(buzzer_id, pulse_count, period, True)
        if not success:
            print(f"buzzers.set_impulses({buzzer_id}, {pulse_count}, {period}) failed")
        time.sleep(pulse_count * period)

    input("Press enter to exit")


def test_qualisys_connection() -> None:
    async def _test_connection():
        connection = await qtm.connect(config.qualisys_ntb_ip, version=config.qtm_rt_version, timeout=5)
        if connection is None:
            print("Unable to establish connection to the Qualisys system.")
            return False

        async with qtm.TakeControl(connection, "password"):
            # Create new recording
            await connection.new()
            await connection.await_event(qtm.QRTEvent.EventConnected)

            # Start recording
            await connection.start()
            await connection.await_event(qtm.QRTEvent.EventCaptureStarted)

            print("Recording for 5 seconds ...")
            await asyncio.sleep(5)

            # Stop recording frames
            await connection.stop()
            await connection.await_event(qtm.QRTEvent.EventCaptureStopped)

            # Save recording
            await connection.save("test.qtm", overwrite=True)

            # Close file
            result = await connection.close()
            if result == b"Closing connection":
                await connection.await_event(qtm.QRTEvent.EventConnectionClosed)

        connection.disconnect()

    asyncio.run(_test_connection())
    input("Press enter to exit")


def create_options() -> list:
    """
    Create list of possible tests
    """
    options = [
        ("Play sounds", play_sounds),
        ("Buzz buzzers", test_buzzers),
        ("Test Qualisys connection", test_qualisys_connection),
    ]
    options += [
        ("Exit", exit),
    ]

    return options


if __name__ == "__main__":
    clear_screen()

    # For now, running is always True and the program exits when "Exit" is selected,
    # calling the python build-in method exit().
    while running:
        clear_screen()
        print_header()

        options = create_options()
        print_options(options)

        choice = None
        while choice is None:
            choice = input(f"Your choice [{Color.CYAN}1 - {len(options)}{Color.END}]: ")

            # Parse the choice
            try:
                choice = int(choice)
            except Exception as e:
                print("Invalid choice, try again")
                choice = None
                continue

            if choice < 1 or choice > len(options):
                print("Invalid choice, try again")
                choice = None
                continue

        print("")  # New line for better readability

        # Options are 1-indexed, therefore 1 needs to be subtracted
        # The options are in the form (description, function)
        # [1]() results in function()
        options[choice - 1][1]()

        print("")  # New line for better readability
