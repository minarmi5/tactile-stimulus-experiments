import time
import PyCmdMessenger

from datetime import datetime

from . import config


class Buzzers:
    # List of command names (and formats for their associated arguments). These must
    # be in the same order as in the arduino code.
    commands = [
        ["buzz", "sfff"],
        ["impulse", "sif"],
        ["toggle", "b"],
        ["response", "b"],
    ]

    def __init__(self, port="/dev/rfcomm0", baud_rate=115200, test=False):
        """
        If `test` is set to True, no connection is made, but every method returns True
        """
        self.test = test

        if not self.test:
            # pip3 install PyCmdMessenger
            # Don't forget to call "sudo rfcomm bind 0 00:14:01:03:38:FC 1"
            self.arduino = PyCmdMessenger.ArduinoBoard(device=port, baud_rate=baud_rate)

            # Give bluetooth some time to connect
            time.sleep(3)

            self.messenger = PyCmdMessenger.CmdMessenger(self.arduino, self.commands)

    def check_connection(self) -> bool:
        if self.test:
            return True

        # Test if the connection works
        self.messenger.send("buzz", "A1", 0.9, 200, 0)
        return self.check_response(True)

    def check_response(self, verbose=False) -> bool:
        if self.test:
            return True

        if verbose:
            print(datetime.now(), "Waiting for the response")

        msg = self.messenger.receive()
        if msg == None:
            if verbose:
                print(datetime.now(), "No response from the controller")
            return False

        if msg[1][0] != 1:
            if verbose:
                print(datetime.now(), "Invalid response received")
            return False

        if verbose:
            print(datetime.now(), "Received valid response")

        return True

    def set_vibration(
        self, channel: str, amplitude: float, frequency: float, duration: float, verbose: bool = False
    ) -> bool:
        if self.test:
            return True

        if channel not in config.buzzer_ids:
            print("Error, invalid channel name")
            return False

        if (type(amplitude) != float and type(amplitude) != int) or amplitude < 0:
            print("Error, amplitude must be non-negative number")
            return False

        if (type(frequency) != float and type(frequency) != int) or frequency < 1 or frequency > 1000:
            print("Error, frequency must be number in range [1, 1000] Hz")
            return False

        if (type(duration) != float and type(duration) != int) or duration < 0:
            print("Error, duration must be non-negative number")
            return False

        self.messenger.send("buzz", channel, amplitude, frequency, duration)
        return self.check_response(verbose)

    def set_impulses(self, channel: str, count: int, period: float, verbose: bool = False) -> bool:
        if self.test:
            return True

        if channel not in config.buzzer_ids:
            print("Error, invalid channel name")
            return False

        if (type(count) != int) or count <= 0:
            print("Error, number of pulses must be positive integer")
            return False

        if (type(period) != float and type(period) != int) or period <= 0 or period > 1:
            print("Error, pulse period must be number in range (0,1] seconds")
            return False

        self.messenger.send("impulse", channel, count, period)
        return self.check_response(verbose)

    # def set_buzzer_level(self, buzzer_id: str, level: float, verbose: bool = False) -> bool:
    #     """
    #     Sets the PWM level for the buzzer `buzzer_id`

    #     Parameters
    #     ----------
    #     buzzer_id: str
    #         ID of the buzzer (e.g. "A1", "C3", ...)

    #     level: float
    #         Value between 0.0 (off) and 1.0 (full power)
    #     """
    #     if self.test:
    #         return True

    #     if verbose:
    #         print(datetime.now(), f"Sending the command ({buzzer_id} : {level})")

    #     self.messenger.send(buzzer_id, level)

    #     if verbose:
    #         print(datetime.now(), f"Command sent")

    #     return self.check_response(verbose)
