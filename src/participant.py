import os
import csv
import pandas as pd
from typing import List  # for type hinting, so that List[Block] can be used

from . import config
from .base_class import BaseClass
from .block import Block
from .color import Color


class Participant(BaseClass):
    """
    Class used to represent a participant of the concluded experiments

    Attributes
    ----------
    participant_id : str
        Identificator of the participant

    name : str
        Name of the participant

    gender : {'F', 'M'}
        Gender of the participant
        'F' - female
        'M' - male

    age : int
        Age of the participant

    profession: str
        Profession of the participant

    activity_per_week : float
        Physical activity per week in hours

    handedness : {'R', 'L', 'A'}
        Handedness of the participant
        'R' - right-handed
        'L' - left-handed
        'A' - ambidexterous

    left_hand_length : float
        Length of the left hand in centimeters

    left_forearm_length : float
        Length of the left forearm in centimeters

    left_upper_arm_length : float
        Length of the left upper arm in centimeters

    left_arm_length : float
        Length of the left arm in centimeters

    landmark_1_buzzer_id : str
    landmark_2_buzzer_id : str
    landmark_3_buzzer_id : str
    landmark_4_buzzer_id : str

    note: str
        For holding any other information not specified above
    """

    # self.CSV_COLUMN_NAMES contains column names in the resulting csv's.
    # self.ATTRIBUTE_NAMES contains names of the Participant class attributes in the same order as Participant.CSV_COLUMN_NAMES.
    # Use only these column names instead of hardcoding them to ensure consistency throughout the whole project.
    # This allows the csv's having readable headers (with units, possible values, etc.) and ensuring the data is loaded correctly
    # (instead of loading the columns from the csv only by the position, which could change when analysing the data).

    CSV_COLUMN_NAMES = [
        "participant id",
        "name",
        "gender (F/M)",
        "age",
        "profession",
        "activity per week [h]",
        "handedness (R/L/A)",
        "left hand length [cm]",
        "left forearm length [cm]",
        "left upper arm length [cm]",
        "left arm length [cm]",
        "id of buzzer on landmark 1",
        "id of buzzer on landmark 2",
        "id of buzzer on landmark 3",
        "id of buzzer on landmark 4",
        "note",
    ]

    ATTRIBUTE_NAMES = [
        "participant_id",
        "name",
        "gender",
        "age",
        "profession",
        "activity_per_week",
        "handedness",
        "left_hand_length",
        "left_forearm_length",
        "left_upper_arm_length",
        "left_arm_length",
        "landmark_1_buzzer_id",
        "landmark_2_buzzer_id",
        "landmark_3_buzzer_id",
        "landmark_4_buzzer_id",
        "note",
    ]

    ATTRIBUTE_TYPES = [
        str,  # "participant_id"
        str,  # "name"
        str,  # "gender"
        int,  # "age"
        str,  # "profession"
        float,  # "activity_per_week"
        str,  # "handedness"
        float,  # "left_hand_length"
        float,  # "left_forearm_length"
        float,  # "left_upper_arm_length"
        float,  # "left_arm_length"
        str,  # "landmark_1_buzzer_id"
        str,  # "landmark_2_buzzer_id"
        str,  # "landmark_3_buzzer_id"
        str,  # "landmark_4_buzzer_id"
        str,  # "note"
    ]

    ATTRIBUTE_COUNT = len(ATTRIBUTE_NAMES)

    def __init__(self) -> None:
        self.participant_id: str = None
        self.name: str = None
        self.gender: str = None
        self.age: int = None
        self.profession: str = None
        self.activity_per_week: float = None
        self.handedness: str = None
        self.left_hand_length: float = None
        self.left_forearm_length: float = None
        self.left_upper_arm_length: float = None
        self.left_arm_length: float = None
        self.landmark_1_buzzer_id: str = None
        self.landmark_2_buzzer_id: str = None
        self.landmark_3_buzzer_id: str = None
        self.landmark_4_buzzer_id: str = None
        self.note: str = None

    def get_blocks(self) -> List[Block]:
        """
        Loads all blocks of this participant from the results folder
        and returns them in a single list

        Blocks are loaded again everytime this function is called
        (instead of keeping the information about all completed or
        running blocks inside this class) to remove complexity and
        make sure the information is valid at any time

        Returns
        -------
        blocks: list
            List of blocks saved in this participant's folder
        """

        # Get contents of the participant's folder and select the blocks
        entries = os.listdir(f"{config.results_dir}/{self.participant_id}")
        block_names = [
            e for e in entries if os.path.exists(f"{config.results_dir}/{self.participant_id}/{e}/info.csv")
        ]

        # Try to load each block
        blocks = []
        for block_name in block_names:
            block = Block()
            success = block.load_existing_block(f"{config.results_dir}/{self.participant_id}/{block_name}/info.csv")
            if success:
                block.participant_id = self.participant_id
                blocks.append(block)

        return blocks

    def print_blocks(self) -> None:
        """
        Print all blocks of this participant
        """
        blocks = self.get_blocks()

        if len(blocks) == 0:
            print("No experiments have been performed yet for this participant.")
            return

        print(f"{Color.BOLD}Block summary{Color.END}")

        # Use pandas df for nice printing. Print only a subset of the info
        attrs_names = ("block_id", "block_type", "completed_trials", "total_trials")
        attrs_idx = [Block.ATTRIBUTE_NAMES.index(name) for name in attrs_names]

        data = {}
        for i in attrs_idx:
            key = Block.CSV_COLUMN_NAMES[i]
            data[key] = []
            for b in blocks:
                data[key].append(getattr(b, Block.ATTRIBUTE_NAMES[i]))

        df = pd.DataFrame(data=data)
        df.set_index(Block.CSV_COLUMN_NAMES[attrs_idx[0]], inplace=True)
        print(df)


def add_participant() -> Participant:
    """
    Asks the user to input participant attributes listed in Participant.ATTRIBUTE_NAMES .

    No value checking is performed, it is expected that the user ensures the validity of the data

    Returns
    -------
    Participant
    """
    new_participant = Participant()
    attribute_id = 0
    while attribute_id < Participant.ATTRIBUTE_COUNT:
        attr_name = Participant.ATTRIBUTE_NAMES[attribute_id]

        # Set the default for "left_arm_length" as the sum
        if attr_name == "left_arm_length":
            length_sum = (
                new_participant.left_hand_length
                + new_participant.left_forearm_length
                + new_participant.left_upper_arm_length
            )
            print(
                f"{Participant.CSV_COLUMN_NAMES[attribute_id]} (leave empty for default "
                + f"{new_participant.left_hand_length:.2f} + "
                + f"{new_participant.left_forearm_length:.2f} + "
                + f"{new_participant.left_upper_arm_length:.2f} = {length_sum:.2f} cm): ",
                end="",
            )
            value = input()

            if value == "":
                value = length_sum
        else:
            print(f"{Participant.CSV_COLUMN_NAMES[attribute_id]}: ", end="")
            value = input()

        # Check data type
        try:
            attr_value = Participant.ATTRIBUTE_TYPES[attribute_id](value)
        except Exception as e:
            print(
                f"Entered value '{value}' cannot be converted to type '{Participant.ATTRIBUTE_TYPES[attribute_id]}'. Try again..."
            )
        else:
            setattr(new_participant, attr_name, attr_value)
            attribute_id += 1

    print("\nCreated a new user with the following data")
    print(new_participant)

    return new_participant
