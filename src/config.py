# Contains contains and configuration of the experiments
import os
from typing import List

###############################################################################################
# FOLDERS AND PATHS
###############################################################################################

# Project root is set to the parent directory of `src/`, containing this file
# i.e., will contain the path "/absolute/path/to/tactile-stimulus-experiments"
project_root: str = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))

# Folder containing all app data (sounds, results, ...)
data_dir: str = os.path.join(project_root, "data")

# Folder containing the experiment results - set it to whatever folder you want your results in
# (needs not to be in "data/", not even inside the project directory)
results_dir: str = os.path.join(data_dir, "experiments")

###############################################################################################
# BUZZERS
###############################################################################################

# Do not change unless the names are changed in the code running on the buzzer board as well
buzzer_ids: list = ["A1", "A2", "A3", "A4", "B1", "B2", "B3", "B4", "C1", "C2", "C3", "C4", "D1", "D2", "D3", "D4"]
# buzzer_ids: list = ["D1", "D2", "D3", "D4"]

buzzer_count: int = len(buzzer_ids)

# Buzzer strength in the 'vibration' mode [0.0, 1.0]
buzzer_vibration_amplitude = 0.4

# Buzzer frequency in the 'vibration' mode
buzzer_vibration_frequency = 200  # [Hz]

# Buzzer pulse period in the 'pulse' mode
buzzer_pulse_period = 0.1  # [s]

###############################################################################################
# EXPERIMENT SETUP
###############################################################################################

# How many times will a single buzzer buzz in a single block
location_repetitions_in_one_block: int = 1

# With `buzzer_count` buzzers, this amounts to
# `buzzer_count` * `location_repetitions_in_one_block` trials in a single block
trials_in_one_block: int = buzzer_count * location_repetitions_in_one_block

# Duration of a single trial in seconds
trial_duration: float = 3

# Time between end of one trial and start of another in seconds
time_between_trials: float = 0.5

# After how many trials a pause is taken. The list can contain multiple values, meaning
# more pauses will be taken during a single block. Default is one pause in the middle.
# Set to an empty list for no pause
pause_after_trials: list = [trials_in_one_block // 2]

# How long the buzzer buzzes in the SHORT block type
buzzer_duration_short: float = 0.3

# Delay between the start of the trial and buzzing the buzzer in seconds.
# The delay is selected randomly in the specified range.
buzzer_delay_range: List[float] = [0.2, 1.8]

###############################################################################################
# SOUNDS
###############################################################################################

# Paths to the sound files
sounds_dir: str = os.path.join(data_dir, "sounds")

# Sound played when the trial starts - files can be either .mp3 or .wav

# After this sound, there is random pause and then the buzzer starts buzzing
start_trial_sound_file: str = os.path.join(sounds_dir, "4_start_trial.wav")

# Time between starting the sound and starting the trial timer in seconds.
# e.g. if the trial should start after some countdown, set this to the time between starting
# the countdown and moment at which the trials starts
start_trial_sound_duration: float = 1.0

# Sound played when the participant can start moving his arm
start_movement_sound_file: str = os.path.join(sounds_dir, "4_start_movement.wav")

# Sound played when the trial ends
end_trial_sound_file: str = os.path.join(sounds_dir, "4_end_trial.wav")

# Duration of the end files in seconds
# The end trial sound is played and after this duration, a command is sent to stop recording
end_trial_sound_duration: float = 2.0

###############################################################################################
# QUALISYS
###############################################################################################

qualisys_ntb_ip: str = "169.254.199.151"
qtm_rt_version: str = "1.21"

qtm_event_trial_start: str = "TRIAL_START"
qtm_event_trial_stop: str = "TRIAL_STOP"
qtm_event_buzzer_start_sent: str = "BUZZER_START_SENT"
qtm_event_buzzer_start_received: str = "BUZZER_START_RECEIVED"
qtm_event_buzzer_stop: str = "BUZZER_STOP"
qtm_event_movement_start: str = "MOVEMENT_START"


###############################################################################################
# UTILITIES & OTHERS
###############################################################################################

# Local timezone for timezone-aware datetime handling
import pytz

tz = pytz.timezone("Europe/Prague")

from .color import Color


def print_config():
    """
    Prints selected subset of the configured values
    """
    config_info = [
        ("Participant data and results directory", results_dir),
        ("Number of buzzers", buzzer_count),
        ("Location repetitions in one block", location_repetitions_in_one_block),
        ("Trials in one block", trials_in_one_block),
        ("Duration of a single trial [s]", trial_duration),
        ("Time between two trials [s]", time_between_trials),
        ("Buzzer duration in SHORT block type [s]", buzzer_duration_short),
        ("Take a pause after trials", pause_after_trials),
    ]
    l = max([len(i[0]) for i in config_info])
    for text, value in config_info:
        # Align the output by <{l} (left alignment to fixed width of l)
        print(f"{text:<{l}} : {Color.BOLD}{value}{Color.END}")
