import os
import csv
import pandas as pd

from .color import Color


class BaseClass:
    CSV_COLUMN_NAMES = []
    ATTRIBUTE_NAMES = []
    ATTRIBUTE_COUNT = len(ATTRIBUTE_NAMES)
    ATTRIBUTE_TYPES = None  # Type checking is performed only if the class defines ATTRIBUTE_TYPES

    def __repr__(self) -> str:
        # Get the longest attribute name length
        l = max([len(self.CSV_COLUMN_NAMES[i]) for i in range(self.ATTRIBUTE_COUNT)], default=0)

        res = []
        for attribute_id in range(self.ATTRIBUTE_COUNT):
            # Align the output by <{l} (left alignment to fixed width of l)
            res.append(
                f"{self.CSV_COLUMN_NAMES[attribute_id]:<{l}} : {Color.BOLD}{getattr(self, self.ATTRIBUTE_NAMES[attribute_id])}{Color.END}"
            )

        return "\n".join(res)

    def load_from_csv(self, filename: str, verbose: bool = False) -> bool:
        """
        Returns
        -------
        successful : bool
            Whether the data were successfuly loaded from file `filename`
        """
        try:
            df = pd.read_csv(filename)
        except Exception as e:
            print(f"Pandas was unable to parse file {filename} as csv")
            print(e)
            print(f"Unable to parse file {filename}, aborting")
            return False

        for col in df.columns:
            if col not in self.CSV_COLUMN_NAMES:
                print(f"{col} is not a valid column name, should be one of the following:")
                print(f"{self.CSV_COLUMN_NAMES}")
                print(f"Unable to parse file {filename}, aborting")
                return False

            # Get the corresponding attribute name for the csv column
            attribute_id = self.CSV_COLUMN_NAMES.index(col)

            # Check the data type and convert if possible
            if self.ATTRIBUTE_TYPES is not None:
                try:
                    value = self.ATTRIBUTE_TYPES[attribute_id](df.at[0, col])
                except Exception as e:
                    print(
                        f"Value '{value}' loaded for attribute '{self.ATTRIBUTE_NAMES[attribute_id]}' cannot be converted to type '{self.ATTRIBUTE_TYPES[attribute_id]}'"
                    )
                    print(f"Unable to parse file {filename}, aborting")
                    return False
                else:
                    setattr(self, self.ATTRIBUTE_NAMES[attribute_id], value)
            else:
                # Set the corresponding attribute in this class
                setattr(self, self.ATTRIBUTE_NAMES[attribute_id], df.at[0, col])

        if verbose:
            print(f"Successfully loaded file {filename}:")
            print(self)

        return True

    def save_to_csv(self, filename: str) -> bool:
        """ """
        try:
            os.makedirs(os.path.dirname(filename), exist_ok=True)
        except Exception as e:
            print(f"Unable to save the participant")
            print(f"{e}")
            return False

        data = {}
        for attribute_id in range(self.ATTRIBUTE_COUNT):
            # The value in the dictionary has to be a list in order for pandas to load the data with the key as the column name
            # and the value as the column values
            # e.g. {'col1' : ['val1']} will result in a dataframe with one column 'col1',containing one data row with value 'val1'
            data[self.CSV_COLUMN_NAMES[attribute_id]] = [getattr(self, self.ATTRIBUTE_NAMES[attribute_id])]

        df = pd.DataFrame(data=data)
        df.to_csv(filename, index=False, quoting=csv.QUOTE_NONNUMERIC)

        return True

    def append_to_csv(self, filename: str) -> bool:
        """
        Adds a row containing information about this object to csv at `filename`

        This script checks whether the file exists.
        If yes, it checks whether the header is valid for the given data and inserts the row.
        If not, the method `save_to_csv` is called instead

        Returns
        -------
        success : bool
        """
        # Check existence
        if not os.path.exists(filename):
            return self.save_to_csv(filename)

        # Check headers
        with open(filename, mode="r", newline="") as f:
            csv_reader = csv.DictReader(f)
            if csv_reader.fieldnames != self.CSV_COLUMN_NAMES:
                print(f"Unable to append data to {filename}, the headers do not match")
                print(f"Existing header: {csv_reader.fieldnames}")
                print(f"Required header: {self.CSV_COLUMN_NAMES}")
                return False

        # Everything seems ok, append the row
        with open(filename, mode="a", newline="") as f:
            csv_writer = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)

            row = []
            for attribute_id in range(self.ATTRIBUTE_COUNT):
                row.append(getattr(self, self.ATTRIBUTE_NAMES[attribute_id]))

            csv_writer.writerow(row)

        return True
