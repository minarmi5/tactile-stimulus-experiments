import time
import random
import asyncio

from datetime import datetime

from qtm.protocol import QRTCommandException


# Create BlockType before importing all other local files to avoid
# errors due to circular imports
class BlockType:
    """
    Enumeration of possible block types

    Values
    ------
    SHORT       Buzzer will buzz shortly, participant can start after the buzzing ended
    CONTINUOUS  Buzzer will buzz continuously, participant can start whenever he wants
    """

    SHORT = "S"
    CONTINUOUS = "C"


class BlockBuzzingMode:
    VIBRATIONS = "V"
    PULSES = "P"


from . import config
from .base_class import BaseClass
from .trial import Trial
from .buzzers import Buzzers


class Block(BaseClass):
    """
    Class used to represent a single block of the concluded experiments

    Attributes
    ----------
    block_id : int
        Numerical ID of the block

    block_type : BlockType
        Type of the block from the enumeration BlockType

    completed_trials : int
        Counts the trials completed, useful when the block is interrupted by some error.
        Starts at 0 for a new block or at some positive value for an interrupted block.
        Is incremented after the whole trial has finished

    total_trials : int
        Total trials that should be concluded in this block

    start_datetime : datetime.datetime
        Timezone-aware object containing the start of this block

    end_datetime : datetime.datetime
        Timezone-aware object containing the end of this block

    seed : int
        Seed used to generate the buzzers order in this block.
    """

    # self.CSV_COLUMN_NAMES contains column names in the resulting csv's.
    # self.ATTRIBUTE_NAMES contains names of the Block class attributes in the same order as Block.CSV_COLUMN_NAMES.
    # Use only these column names instead of hardcoding them to ensure consistency throughout the whole project.
    # This allows the csv's having readable headers (with units, possible values, etc.) and ensuring the data is loaded correctly
    # (instead of loading the columns from the csv only by the position, which could change when analysing the data).

    ATTRIBUTE_NAMES = [
        "block_id",
        "block_type",
        "block_buzzing_mode",
        "completed_trials",
        "total_trials",
        "start_datetime",
        "end_datetime",
        "seed",
    ]

    CSV_COLUMN_NAMES = [
        "block id",
        "block type",
        "block buzzing mode",
        "completed trials",
        "total trials",
        "start datetime",
        "end datetime",
        "seed",
    ]

    ATTRIBUTE_COUNT = len(ATTRIBUTE_NAMES)

    def __init__(self) -> None:
        self.block_id: int = None
        self.block_type: BlockType = None
        self.block_buzzing_mode: BlockBuzzingMode = None

        self.completed_trials: int = None
        self.total_trials: int = None

        self.start_datetime: datetime = None
        self.end_datetime: datetime = None

        self.seed: int = None

        # Other attributes which are not saved in the CSV
        # (and are therefore not in ATTRIBUTE_NAMES or CSV_COLUMN_NAMES)
        self.participant_id: int = None
        self.ready: bool = False

    def initialize_new_block(
        self,
        participant_id: int,
        block_id: int,
        block_type: BlockType,
        block_buzzing_mode: BlockBuzzingMode,
        total_trials: int,
        seed: int,
        save: bool = True,
    ) -> bool:
        """
        Parameters
        ----------
        participant_id: int
        block_id: int
        block_type: block.BlockType
        block_buzzing_mode: block.blockBuzzingMode
        total_trials: int
        seed: int

        save: bool
            Whether to save the trials of this block to the result files (default = True)

        Returns
        -------
        ready : bool
            Whether the block is ready to be run by block.run_block()
        """
        self.participant_id = participant_id

        self.block_id = block_id
        self.block_type = block_type
        self.block_buzzing_mode = block_buzzing_mode

        self.completed_trials = 0
        self.total_trials = total_trials

        # Set the seed and generate the order of the buzzers
        self.seed = seed
        random.seed(self.seed)
        # sample(x, k=len(x)) returns shuffled x, without modifying it
        # k * ["A1", "A2", ...] creates a list where each element is contained k-times
        self.buzzers_order = random.sample(
            config.location_repetitions_in_one_block * config.buzzer_ids,
            k=config.location_repetitions_in_one_block * config.buzzer_count,
        )

        # Reset the seed!
        random.seed(None)

        # The block is saved to the result directory - in case the block is interrupted while
        # it is running, it can be reloaded from this file (by self.load_existing_block) and continued
        if save:
            self.save_to_csv(f"{config.results_dir}/{self.participant_id}/block_{self.block_id}/info.csv")
        self.ready = True
        return self.ready

    def load_existing_block(self, filename: str) -> bool:
        """
        Loads block data from the corresponding file `info.csv`.

        In the case that the block has not been finished,
        block.completed_trials will be less then block.total_trials
        and the user can continue with performing this block by
        selecting 'Resume an interrupted block' in the main menu.

        The file `info.csv` has to have the same columns as the
        current value of self.CSV_COLUMN_NAMES, otherwise the file
        is not loaded.

        Parameters
        ----------
        filename: str
            Path to the file `info.csv` of the corresponing block

        Returns
        -------
        success: bool
            Whether the file has been loaded correctly
        """
        # Checking the validity of the file is done in the method `load_from_csv`
        # inherited from the base class BaseClass
        self.ready = self.load_from_csv(filename)

        # Generate the buzzer order based on the seed
        random.seed(self.seed)
        # sample(x, k=len(x)) returns shuffled x, without modifying it
        # k * ["A1", "A2", ...] creates a list where each element is contained k-times
        self.buzzers_order = random.sample(
            config.location_repetitions_in_one_block * config.buzzer_ids,
            k=config.location_repetitions_in_one_block * config.buzzer_count,
        )

        # Reset the seed!
        random.seed(None)

        return self.ready

    def run_block(self, buzzers: Buzzers, save: bool = True) -> bool:
        """
        Runs the trials in this block one after another, until the last trial has been finished
        or the experiment was interrupted.

        All block data are saved to files `info.csv` and `trials.csv` in the corresponding result folder.
        Both files are updated after every finished trial, in case the experiment is interrupted.
        When this happens, the block can be resumed by selecting 'Resume an interrupted block' in the main menu.

        Parameters
        ----------
        buzzers: Buzzers
            Object containing connection to the buzzer board

        save: bool
            Whether to save the trials of this block to the result files (default = True)

        Returns
        -------
        success: bool
            Whether the block has finished successfuly
        """
        if not self.ready:
            print(f"Block {self.block_id} of type '{self.block_type}' is not ready to be started.")
            print("Prepare the block by either initializing new one or loading an existing one")
            return False

        self.start_datetime = datetime.now(tz=config.tz)

        # Run the trials until the specified number of trials was reached
        # completed_trials starts at 0 when initialized by `initialize_new_block`
        # or continues from a specific value if the block was loaded by `load_existing_block`
        while self.completed_trials < self.total_trials:
            trial = Trial()

            # Setup the trial
            trial.trial_id = self.completed_trials + 1  # Convert from 0-indexing to 1-indexing
            trial.buzzer_id = self.buzzers_order[self.completed_trials]  # or trial.trial_id - 1
            trial.trial_seed = self.seed + trial.trial_id

            # Run the trial - start and end datetime, duration and distance are computed inside the trial
            # Due to QTM connection being unstable sometimes, we try multiple times
            for i in range(3):
                try:
                    result = trial.run_trial(
                        self.block_type,
                        self.block_buzzing_mode,
                        buzzers,
                        f"{self.participant_id}_{self.block_id:02d}_{trial.trial_id:03d}.qtm",
                        save,
                    )
                
                except (QRTCommandException, asyncio.exceptions.CancelledError, asyncio.exceptions.TimeoutError) as e:
                    print(f"Exception occured while running trial {trial.trial_id}:\n{e}")
                    result = False

                if not result:
                    if i == 0:
                        print(f"Trial {trial.trial_id} failed, trying again (2/3 tries)...")
                        time.sleep(1)
                    if i == 1:
                        print(
                            "Unable to complete trial. Please check that QTM is connected and in the correct state (gray window without camera feeds) and press ENTER."
                        )
                        input("")
                        print(f"Trying again (3/3 tries)...")
                        time.sleep(1)
                    if i == 2:
                        print("Unable to complete trial, aborting.")
                        return False
                else:
                    break

            if save:
                trial.append_to_csv(f"{config.results_dir}/{self.participant_id}/block_{self.block_id}/trials.csv")

            self.completed_trials += 1

            # Update the csv
            if save:
                self.save_to_csv(f"{config.results_dir}/{self.participant_id}/block_{self.block_id}/info.csv")

            # Take a pause if user specified it in config.pause_after_trials
            if self.completed_trials in config.pause_after_trials:
                input("Experiment was paused. Press enter to continue with the trials ...")

            else:
                time.sleep(config.time_between_trials)

        return True
