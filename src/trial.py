import qtm
import random
import pygame
import asyncio

from datetime import datetime, timedelta

from . import config
from .block import BlockType, BlockBuzzingMode

from .base_class import BaseClass
from .buzzers import Buzzers


class Trial(BaseClass):
    """
    Class used to represent a single trial of the concluded experiments

    Attributes
    ----------
    trial_id : int
        Numerical ID of the trial

    buzzer_id : str
        Which buzzer is used in this trial

    trial_start_dt : datetime.datetime
    trial_end_dt : datetime.datetime

    buzzer_start_sent_dt : datetime.datetime
        Datetime at which the buzzer was commanded to start

    buzzer_start_received_dt : datetime.datetime
        Datetime at which we received response that the buzzer started successfuly

    buzzer_stop_dt : datetime.datetime
        Datetime computed as 'buzzer_start_sent_dt' + duration of commanded action

    movement_allowed_dt : datetime.datetime

    trial_seed : int
        Seed used to generate all delay durations, normally set to `block seed` + `trial_id`
    """

    # self.CSV_COLUMN_NAMES contains column names in the resulting csv's.
    # self.ATTRIBUTE_NAMES contains names of the Trial class attributes in the same order as Trial.CSV_COLUMN_NAMES.
    # Use only these column names instead of hardcoding them to ensure consistency throughout the whole project.
    # This allows the csv's having readable headers (with units, possible values, etc.) and ensuring the data is loaded correctly
    # (instead of loading the columns from the csv only by the position, which could change when analysing the data).

    ATTRIBUTE_NAMES = [
        "trial_id",
        "buzzer_id",
        "trial_start_dt",
        "trial_end_dt",
        "buzzer_start_sent_dt",
        "buzzer_start_received_dt",
        "buzzer_stop_dt",
        "movement_allowed_dt",
        "trial_seed",
    ]

    CSV_COLUMN_NAMES = [
        "trial id",
        "buzzer id",
        "trial start",
        "trial end",
        "buzzer start sent",
        "buzzer start received",
        "buzzer stop",
        "movement allowed",
        "trial seed",
    ]

    ATTRIBUTE_COUNT = len(ATTRIBUTE_NAMES)

    def __init__(self) -> None:
        self.trial_id: int = None
        self.buzzer_id: str = None

        self.trial_start_dt: datetime = None
        self.trial_end_dt: datetime = None

        self.buzzer_start_sent_dt: datetime = None
        self.buzzer_start_received_dt: datetime = None
        self.buzzer_stop_dt: datetime = None

        self.movement_allowed_dt: datetime = None

        self.trial_seed: int = None

    async def _start_buzzer_vibration(self, connection: qtm.QRTConnection, buzzers: Buzzers, duration: float) -> bool:
        # Commanding the buzzer has some latency, we need to measure
        # and save the delay between sending a command and receiving
        # an answer in order to account for it later
        await connection.set_qtm_event(config.qtm_event_buzzer_start_sent)
        self.buzzer_start_sent_dt = datetime.now(tz=config.tz)

        amplitude = config.buzzer_vibration_amplitude
        frequency = config.buzzer_vibration_frequency

        success = buzzers.set_vibration(self.buzzer_id, amplitude, frequency, duration)

        await connection.set_qtm_event(config.qtm_event_buzzer_start_received)
        self.buzzer_start_received_dt = datetime.now(tz=config.tz)

        if success:
            print(f"\t[{datetime.now(tz=config.tz)}] Buzzer {self.buzzer_id} was started")
            return True
        else:
            print(f"\t[{datetime.now(tz=config.tz)}] Buzzer {self.buzzer_id} couldn't be started")
            return False

    async def _start_buzzer_pulses(self, connection: qtm.QRTConnection, buzzers: Buzzers, duration: float) -> bool:
        # Commanding the buzzer has some latency, we need to measure
        # and save the delay between sending a command and receiving
        # an answer in order to account for it later
        await connection.set_qtm_event(config.qtm_event_buzzer_start_sent)
        self.buzzer_start_sent_dt = datetime.now(tz=config.tz)

        period = config.buzzer_pulse_period
        pulse_count = int(duration / period)

        success = buzzers.set_impulses(self.buzzer_id, pulse_count, period)

        await connection.set_qtm_event(config.qtm_event_buzzer_start_received)
        self.buzzer_start_received_dt = datetime.now(tz=config.tz)

        if success:
            print(f"\t[{datetime.now(tz=config.tz)}] Buzzer {self.buzzer_id} was started")
            return True
        else:
            print(f"\t[{datetime.now(tz=config.tz)}] Buzzer {self.buzzer_id} couldn't be started")
            return False

    async def _start_trial(self, connection: qtm.QRTConnection) -> bool:
        """
        Plays the start trial sound, send the START_TRIAL event to qualisys and
        sets self.trial_start_dt.

        The sound is played in a non-blocking mode, meaning the START_TRIAL event
        is sent and the trial_start_dt is set as soon as the sound starts. If you
        want to start the trial during or after the sound, specify this delay in
        `config.start_trial_sound_duration`.

        Parameters
        ----------
        connection: qtm.QRTConnection
            Active connection to the Qualisys system

        Returns
        -------
        success: bool
        """
        pygame.mixer.music.load(config.start_trial_sound_file)
        pygame.mixer.music.play()

        await asyncio.sleep(config.start_trial_sound_duration)

        await connection.set_qtm_event(config.qtm_event_trial_start)
        self.trial_start_dt = datetime.now(tz=config.tz)

        return True

    async def _start_buzzing_after_random_delay(
        self, block_buzzing_mode: BlockBuzzingMode, duration: float, connection: qtm.QRTConnection, buzzers: Buzzers
    ) -> bool:
        """
        Waits for a random time interval (specified by config.buzzer_delay_range)
        and buzzes the buzzer for 'duration'

        Parameters
        ----------
        connection: qtm.QRTConnection
            Active connection to the Qualisys system

        buzzers: Buzzers
            Object containing connection to the buzzer board

        Returns
        -------
        success: bool
        """
        # random.random generates float in the interval [0, 1)
        # use this number to compute linear interpolation between
        # the lower and upper bound of the buzzer delay as
        # (1-t)*lower_bound + t*upper_bound, mapping [0, 1) to [lower_bound, upper_bound)
        t = random.random()
        delay = (1 - t) * config.buzzer_delay_range[0] + t * config.buzzer_delay_range[1]

        print(f"\t[{datetime.now(tz=config.tz)}] Buzzer will start buzzing in {delay:.3f} seconds")
        await asyncio.sleep(delay)

        if block_buzzing_mode == BlockBuzzingMode.VIBRATIONS:
            return await self._start_buzzer_vibration(connection, buzzers, duration)

        if block_buzzing_mode == BlockBuzzingMode.PULSES:
            return await self._start_buzzer_pulses(connection, buzzers, duration)

    async def _handle_short_trial_type(
        self, block_buzzing_mode: BlockBuzzingMode, connection: qtm.QRTConnection, buzzers: Buzzers
    ) -> bool:
        """
        For BlockType.SHORT, the buzzer is stopped after a while and only then
        can the participant start his motion

        Temporary stimulus
        Stimulus start |________________________| Stimulus end
                                     Response start |______________________| Response end

        Parameters
        ----------
        connection: qtm.QRTConnection
            Active connection to the Qualisys system

        buzzers: Buzzers
            Object containing connection to the buzzer board

        Returns
        -------
        success: bool
        """
        success = await self._start_buzzing_after_random_delay(
            block_buzzing_mode, config.buzzer_duration_short, connection, buzzers
        )
        if not success:
            print("Error occured in `trial._start_buzzing_after_random_delay`, aborting the trial")
            return False

        # Sleep so that time between self.buzzer_start_sent_dt and self.buzzer_stop_dt
        # is exactly config.buzzer_duration_short
        remaining_td = timedelta(seconds=config.buzzer_duration_short) - (
            datetime.now(tz=config.tz) - self.buzzer_start_sent_dt
        )
        remaining_time = remaining_td.total_seconds()
        if remaining_time > 0:  # sanity check, should not happen
            await asyncio.sleep(remaining_time)

        # Mark the expected time of the end of buzzing
        await connection.set_qtm_event(config.qtm_event_buzzer_stop)
        self.buzzer_stop_dt = datetime.now(tz=config.tz)

        # Allow movement after buzzer stopped
        await connection.set_qtm_event(config.qtm_event_movement_start)
        self.movement_allowed_dt = datetime.now(tz=config.tz)

        pygame.mixer.music.load(config.start_movement_sound_file)
        pygame.mixer.music.play()

        # There is nothing else to do until the end of the trial, so sleep until then
        # Time between self.buzzer_start_sent_dt and self.trial_end_dt will be config.trial_duration
        remaining_td = timedelta(seconds=config.trial_duration) - (
            datetime.now(tz=config.tz) - self.buzzer_start_sent_dt
        )
        remaining_time = remaining_td.total_seconds()
        if remaining_time > 0:  # sanity check, should not happen
            await asyncio.sleep(remaining_time)

        return True

    async def _handle_continuous_trial_type(
        self, block_buzzing_mode: BlockBuzzingMode, connection: qtm.QRTConnection, buzzers: Buzzers
    ) -> bool:
        """
        For BlockType.CONTINUOUS, the buzzer keeps buzzing until the end of the trial

        Stimulus start |______________________________________________| Stimulus end
                                Response start |______________________| Response end

        Parameters
        ----------
        connection: qtm.QRTConnection
            Active connection to the Qualisys system

        buzzers: Buzzers
            Object containing connection to the buzzer board

        Returns
        -------
        success: bool
        """
        print("Running continuous block")
        success = await self._start_buzzing_after_random_delay(
            block_buzzing_mode, config.trial_duration, connection, buzzers
        )
        if not success:
            print("Error occured in `trial._start_buzzing_after_random_delay`, aborting the trial")
            return False

        await connection.set_qtm_event(config.qtm_event_movement_start)
        self.movement_allowed_dt = datetime.now(tz=config.tz)

        pygame.mixer.music.load(config.start_movement_sound_file)
        pygame.mixer.music.play()

        # There is nothing else to do until the end of the trial, so sleep until then
        remaining_td = timedelta(seconds=config.trial_duration) - (
            datetime.now(tz=config.tz) - self.buzzer_start_sent_dt
        )
        remaining_time = remaining_td.total_seconds()
        if remaining_time > 0:  # sanity check, should not happen
            await asyncio.sleep(remaining_time)

        # Mark the expected time of the end of buzzing
        await connection.set_qtm_event(config.qtm_event_buzzer_stop)
        self.buzzer_stop_dt = datetime.now(tz=config.tz)

        return True

    async def _end_trial(self, connection: qtm.QRTConnection) -> bool:
        """
        Parameters
        ----------
        connection: qtm.QRTConnection
            Active connection to the Qualisys system

        Returns
        -------
        success: bool
        """
        # Finish the trial
        await connection.set_qtm_event(config.qtm_event_trial_stop)
        self.trial_end_dt = datetime.now(tz=config.tz)

        pygame.mixer.music.load(config.end_trial_sound_file)
        pygame.mixer.music.play()

        await asyncio.sleep(config.end_trial_sound_duration)
        return True

    async def _run_trial(
        self,
        block_type: BlockType,
        block_buzzing_mode: BlockBuzzingMode,
        buzzers: Buzzers,
        qualisys_ntb_filename: str,
        save: bool = True,
    ) -> bool:
        """
        Coroutine handling the experiment trial. Needs to be
        asynchronous since the qtm package uses asyncio
        to communicate

        This function is not meant to be called directly by the user.
        To run a single trial call Trial.run_trial() instead.

        Parameters
        ----------
        block_type: BlockType
            The type of the block, in which this trial is run,
            This modifies the behavior of the trial
            (when the buzzer starts/stops buzzing, when is movement
            allowed, etc.)

        buzzers: Buzzers
            Object containing connection to the buzzer board

        qualisys_ntb_filename: str
            File to which the recording will be saved (on the qualisys notebook)

        Returns
        -------
        success: bool
            Whether the trial finished successfuly
        """
        # Connect to the qualisys motion capture system. If it is not possible,
        # do not continue with the trial
        connection = await qtm.connect(config.qualisys_ntb_ip, version=config.qtm_rt_version, timeout=20)
        if connection is None:
            print("Unable to establish connection to the Qualisys system.")
            return False

        async with qtm.TakeControl(connection, "password"):
            # First of all, start the recording
            # Create new recording
            await connection.new()
            await connection.await_event(qtm.QRTEvent.EventConnected)

            # Start recording
            await connection.start()
            await connection.await_event(qtm.QRTEvent.EventCaptureStarted)

            success = await self._start_trial(connection)
            if not success:
                print("Error occured in `trial._start_trial`, aborting the trial")
                return False

            # Select behavior based on the trial type
            if block_type == BlockType.CONTINUOUS:
                success = await self._handle_continuous_trial_type(block_buzzing_mode, connection, buzzers)
                if not success:
                    print("Error occured in `trial._handle_continuous_trial_type`, aborting the trial")
                    return False

            elif block_type == BlockType.SHORT:
                success = await self._handle_short_trial_type(block_buzzing_mode, connection, buzzers)
                if not success:
                    print("Error occured in `trial._handle_short_trial_type`, aborting the trial")
                    return False

            success = await self._end_trial(connection)
            if not success:
                print("Error occured in `trial._end_trial`, aborting the trial")
                return False

            # Stop recording frames
            await connection.stop()
            await connection.await_event(qtm.QRTEvent.EventCaptureStopped)

            # Save recording
            if save:
                await connection.save(qualisys_ntb_filename, overwrite=True)
            else:
                # The qualisys system leaves the window opened when the measurement
                # is not saved, so save to a temporary file instead
                await connection.save("test.qtm", overwrite=True)

            # Close file
            result = await connection.close()
            if result == b"Closing connection":
                await connection.await_event(qtm.QRTEvent.EventConnectionClosed)

        connection.disconnect()
        return True

    def run_trial(
        self,
        block_type: BlockType,
        block_buzzing_mode: BlockBuzzingMode,
        buzzers: Buzzers,
        qualisys_ntb_filename: str,
        save: bool = True,
    ) -> bool:
        """
        Parameters
        ----------
        block_type: BlockType
            The type of the block, in which this trial is run,
            This modifies the behavior of the trial
            (when the buzzer starts/stops buzzing, when is movement
            allowed, etc.)

        buzzers: Buzzers
            Object containing connection to the buzzer board

        qualisys_ntb_filename: str
            File to which the recording will be saved (on the qualisys notebook)

        Returns
        -------
        success: bool
            Whether the trial finished successfuly
        """
        # Set the trial seed to ensure the trials are repeatable
        random.seed(self.trial_seed)
        print(f"Running trial {self.trial_id} (with buzzer {self.buzzer_id}) ...")

        # Run the coroutine - asyncio.run handles everything (event loops, ...)
        success = asyncio.run(self._run_trial(block_type, block_buzzing_mode, buzzers, qualisys_ntb_filename, save))

        # Reset the seed!
        random.seed(None)

        if not success:
            print(f"Trial {self.trial_id} failed")
            return False

        # Evaluate the trial results
        print(f"Trial {self.trial_id} finished successfuly")
        return True
